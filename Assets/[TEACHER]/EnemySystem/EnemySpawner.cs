// using System;
// using UnityEngine;
// using Random = UnityEngine.Random;
//
// namespace Sample
// {
//     public sealed class EnemySpawner : MonoBehaviour
//     {
//         [SerializeField]
//         private EnemyService enemyService;
//
//         [SerializeField]
//         private EnemyPool enemyPool;
//         
//         [SerializeField, Space]
//         private float spawnCountdown = 5.0f;
//
//         [SerializeField]
//         private int spawnLimit = 3;
//
//         [SerializeField, Space]
//         private SpawnData[] spawnDataSet;
//         
//         private float spawnTime;
//
//         private void Start()
//         {
//             this.spawnTime = this.spawnCountdown;
//         }
//
//         private void FixedUpdate()
//         {
//             if (this.enemyService.EnemyCount >= this.spawnLimit)
//             {
//                 return;
//             }
//             
//             if (this.spawnTime > 0)
//             {
//                 this.spawnTime -= Time.fixedDeltaTime;
//                 return;
//             }
//
//             this.SpawnEnemy();
//             this.spawnTime = this.spawnCountdown;
//         }
//
//         private void SpawnEnemy()
//         {
//             var spawnData = this.spawnDataSet[Random.Range(0, this.spawnDataSet.Length)];
//
//             var enemy = this.enemyPool.Get();
//             enemy.transform.position = spawnData.spawnPoint.position;
//             enemy.transform.rotation = spawnData.spawnPoint.rotation;
//             enemy.GetComponentInChildren<NPCData>().patrolPoints = spawnData.patrolPoints;
//             enemy.GetComponent<LifeComponent>().SetFullHitPoints();
//             
//             this.enemyService.RegisterEnemy(enemy);
//         }
//         
//         [Serializable]
//         public struct SpawnData
//         {
//             public Transform spawnPoint;
//             public Transform[] patrolPoints;
//         }
//     }
// }
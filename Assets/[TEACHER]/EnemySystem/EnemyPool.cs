// using System.Collections.Generic;
// using UnityEngine;
//
// namespace Sample
// {
//     public sealed class EnemyPool : MonoBehaviour
//     {
//         [SerializeField]
//         private GameObject enemyPrefab;
//
//         [SerializeField]
//         private int initialCount = 0;
//
//         [SerializeField]
//         private Transform poolTransform;
//
//         [SerializeField]
//         private Transform worldTransform;
//         
//         private readonly Queue<GameObject> enemyQueue = new();
//
//         private void Awake()
//         {
//             for (int i = 0; i < initialCount; i++)
//             {
//                 var enemy = Instantiate(this.enemyPrefab, this.poolTransform);
//                 this.enemyQueue.Enqueue(enemy);
//             }
//         }
//
//         public GameObject Get()
//         {
//             if (this.enemyQueue.TryDequeue(out var enemy))
//             {
//                 enemy.transform.SetParent(this.worldTransform);
//                 return enemy;
//             }
//
//             return Instantiate(this.enemyPrefab, this.worldTransform);
//         }
//
//         public void Release(GameObject enemy)
//         {
//             this.enemyQueue.Enqueue(enemy);
//             enemy.transform.SetParent(this.poolTransform);
//         }
//     }
// }
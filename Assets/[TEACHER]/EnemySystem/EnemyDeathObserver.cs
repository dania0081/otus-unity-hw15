// using UnityEngine;
//
// namespace Sample
// {
//     public sealed class EnemyDeathObserver : MonoBehaviour
//     {
//         [SerializeField]
//         private EnemyService enemyService;
//
//         [SerializeField]
//         private EnemyPool enemyPool;
//         
//         private void OnEnable()
//         {
//             this.enemyService.OnRegistered += this.OnEnemyAdded;
//             this.enemyService.OnUnregistered += this.OnEnemyRemoved;
//         }
//
//         private void OnDisable()
//         {
//             this.enemyService.OnRegistered -= this.OnEnemyAdded;
//         }
//
//         private void OnEnemyAdded(GameObject enemy)
//         {
//             enemy.GetComponent<LifeComponent>().OnDeath += this.OnDeath;
//         }
//
//         private void OnEnemyRemoved(GameObject enemy)
//         {
//             enemy.GetComponent<LifeComponent>().OnDeath -= this.OnDeath;
//         }
//
//         private void OnDeath(LifeComponent component)
//         {
//             component.OnDeath -= this.OnDeath;
//             var enemy = component.gameObject;
//             this.enemyService.UnregisterEnemy(enemy);
//             this.enemyPool.Release(enemy);
//         }
//     }
// }
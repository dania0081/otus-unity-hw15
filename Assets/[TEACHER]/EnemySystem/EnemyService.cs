// using System;
// using System.Collections.Generic;
// using UnityEngine;
//
// namespace Sample
// {
//     public sealed class EnemyService : MonoBehaviour
//     {
//         public event Action<GameObject> OnRegistered;
//         public event Action<GameObject> OnUnregistered;
//
//         private readonly HashSet<GameObject> sceneEnemies = new();
//
//         public int EnemyCount
//         {
//             get { return this.sceneEnemies.Count; }
//         }
//
//         private void Start()
//         {
//             var enemies = GameObject.FindGameObjectsWithTag("Enemy");
//             foreach (var enemy in enemies)
//             {
//                 this.RegisterEnemy(enemy);
//             }
//         }
//
//         public void RegisterEnemy(GameObject enemy)
//         {
//             if (this.sceneEnemies.Add(enemy))
//             {
//                 this.OnRegistered?.Invoke(enemy);
//             }
//         }
//
//         public void UnregisterEnemy(GameObject enemy)
//         {
//             if (this.sceneEnemies.Remove(enemy))
//             {
//                 this.OnUnregistered?.Invoke(enemy);
//             }
//         }
//     }
// }
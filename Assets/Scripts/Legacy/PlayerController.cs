using Cinemachine;
using Sample;
using StarterAssets;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _aimCam;
    [SerializeField] private StarterAssetsInputs _input;
    [SerializeField] private Animator animator;
    [SerializeField] private float normalSensitivity;
    [SerializeField] private float aimSensitivity;
    [SerializeField] private ThirdPersonController thirdPersonController;
    [SerializeField] private WeaponComponent weaponComponent;
    [SerializeField] private TextMeshProUGUI weaponTypeText;

    private void Start()
    {
        if (weaponComponent.bulletType is BulletTypes.Rocket)
        {
            weaponTypeText.text = "Ракетница";
        }
        else
        {
            weaponTypeText.text = "Миномет";
        }
    }

    private void Update() 
    {
        var mouseWorldPosition = Vector3.zero;

        if (_input.isAim) 
        {
            _aimCam.gameObject.SetActive(true);
            thirdPersonController.SetSensitivity(aimSensitivity);
            thirdPersonController.SetRotateOnMove(false);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 1f, Time.deltaTime * 13f));

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;

            transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f);
        } 
        else 
        {
            _aimCam.gameObject.SetActive(false);
            thirdPersonController.SetRotateOnMove(true);
            thirdPersonController.SetSensitivity(normalSensitivity);

            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 0f, Time.deltaTime * 13f));
        }

        if (_input.isFire)
        {
            weaponComponent.Shoot();
        }

        if (_input.isSwap)
        {
            if(weaponComponent.bulletType is BulletTypes.Rocket)
            {
                weaponTypeText.text = "Миномет";
                weaponComponent.bulletType = BulletTypes.Mine;
            }
            else
            {
                weaponComponent.bulletType = BulletTypes.Rocket;
                weaponTypeText.text = "Ракетница";
            }
            _input.isSwap = false;
        }
    }

    // private void OnCollisionEnter(Collision collision)
    // {
    //     Destroy(collision.gameObject);
    // }
}


//
// if (fireReloading <= 0)
// {
//     fireReloading = fireSpeed;
//
//     if (Random.Range(0, 1f) > fireAccurate)
//     {
//        var obj = Instantiate(onFireObject, hitPoint, Quaternion.identity);
//         
//         //var obj = Instantiate(bullet, RifleStartBullet, Quaternion.identity);
//         //var bulletObj = obj.GetComponent<Bullet>();
//         //bulletObj.SetEndPos(hitPoint);
//         //bulletObj.StartFly();
//     }
//
//     if (hitTransform != null && hitTransform.TryGetComponent(out Rigidbody rb))
//     {
//         rb.AddExplosionForce(1000, transform.forward, 100);
//     }
//     
// }
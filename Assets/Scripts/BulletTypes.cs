using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletTypes : byte
{
    None = 0,
    Rocket = 1,
    Mine = 2
}

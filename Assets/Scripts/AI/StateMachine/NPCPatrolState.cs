using UnityEngine;

namespace Sample
{
    public sealed class NPCPatrolState : State
    {
        [SerializeField]
        private NPCData data;
        private int _indexOfPatrolPoint;

        public override void OnEnter()
        {
            data.agent.updatePosition = false;
            data.agent.enabled = true;
            
        }

        public override void OnUpdate(float deltaTime)
        {
            var myPosition = data.rootTransform.position;
            var targetPosition = data.patrolPoints[_indexOfPatrolPoint].position;
            var distance = targetPosition - myPosition;
            var shouldMove = distance.magnitude > 0.2f;

            if (shouldMove)
            {
                data.agent.destination = targetPosition;
                data.rootTransform.position = data.agent.nextPosition;
            }
            else
            {
                _indexOfPatrolPoint = (_indexOfPatrolPoint + 1) % data.patrolPoints.Length;
            }

            data.animator.SetFloat(NPCData.animIDSpeed, shouldMove ? 1 : 0);
            data.animator.SetFloat(NPCData.animIDMotionSpeed, shouldMove ? 3 : 0);
        }

        public override void OnExit()
        {
            data.agent.enabled = false;
        }
    }
}
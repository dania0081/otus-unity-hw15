using Sample;
using UnityEngine;

namespace GameSystem
{
    public sealed class PlayerDeathObserver : MonoBehaviour
    {
        [SerializeField]
        private GameObject player;

        [SerializeField]
        private GameManager gameManager;

        private void OnEnable()
        {
            this.player.GetComponent<LifeComponent>().OnDeath += this.OnDeath;
        }

        private void OnDisable()
        {
            this.player.GetComponent<LifeComponent>().OnDeath -= this.OnDeath;
        }

        private void OnDeath(LifeComponent component)
        {
            this.gameManager.LoseGame();
        }
    }
}
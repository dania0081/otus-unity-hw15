using UnityEngine;

namespace GameSystem
{
    public sealed class GameManager : MonoBehaviour
    {
        public void WinGame()
        {
            Debug.Log("Win Game");
            Time.timeScale = 0;
        }

        public void LoseGame()
        {
            Debug.Log("Lose Game");
        }
    }
}
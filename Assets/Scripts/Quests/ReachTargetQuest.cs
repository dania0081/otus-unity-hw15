using GameSystem;
using UnityEngine;

namespace Sample
{
    public sealed class ReachTargetQuest : MonoBehaviour
    {
        [SerializeField, Space]
        private GameObject player;
        
        [SerializeField]
        private Transform destination;
        
        [SerializeField]
        private GameManager gameManager;

        [SerializeField]
        private float stoppingDistance = 1f;
        
        private void FixedUpdate()
        {
            var playerPosition = this.player.transform.position;
            var targetPosition = this.destination.position;
            
            if ((targetPosition - playerPosition).magnitude <= stoppingDistance)
            {
                this.gameManager.WinGame();
            }
        }
    }
}
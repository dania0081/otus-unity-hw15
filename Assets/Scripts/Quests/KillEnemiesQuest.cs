using System;
using Components;
using GameSystem;
using UnityEngine;

namespace Sample
{
    public sealed class KillEnemiesQuest : MonoBehaviour
    {
        [SerializeField]
        private GameManager gameManager;
        
        [SerializeField]
        private GameObject player;

        [SerializeField]
        private int requiredKills = 1;

        [SerializeField]
        private int currentKills = 0;

        private void OnEnable()
        {
            this.player.GetComponent<KillComponent>().OnKilled += this.OnKilled;
        }

        private void OnDisable()
        {
            this.player.GetComponent<KillComponent>().OnKilled -= this.OnKilled;
        }

        private void OnKilled(GameObject target)
        {
            if (!target.CompareTag("Enemy"))
            {
                return;
            }

            this.currentKills++;

            if (this.currentKills < this.requiredKills)
            {
                return;
            }

            this.gameManager.WinGame();
        }
    }
    
}
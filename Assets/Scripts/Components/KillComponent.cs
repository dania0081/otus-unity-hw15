using System;
using UnityEngine;

namespace Components
{
    public sealed class KillComponent : MonoBehaviour
    {
        public event Action<GameObject> OnKilled;

        public void NotifyAboutKill(GameObject target)
        {
            Debug.Log($"KILLED {target.name}");
            this.OnKilled?.Invoke(target);
        }
    }
}
using StarterAssets;
using UnityEngine;

namespace Sample
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(LifeComponent))]
    [RequireComponent(typeof(ThirdPersonController))]
    [RequireComponent(typeof(CharacterController))]
    public sealed class PlayerDeathComponent : MonoBehaviour
    {
        private LifeComponent lifeComponent;
        private Animator animator;
        private ThirdPersonController thirdPersonController;
        private CharacterController characterController;
        
        private void Awake()
        {
            this.lifeComponent = this.GetComponent<LifeComponent>();
            this.animator = this.GetComponent<Animator>();
            this.thirdPersonController = this.GetComponent<ThirdPersonController>();
            this.characterController = this.GetComponent<CharacterController>();
        }

        private void OnEnable()
        {
            this.lifeComponent.OnDeath += this.OnDeath;
        }

        private void OnDisable()
        {
            this.lifeComponent.OnDeath -= this.OnDeath;
        }

        private void OnDeath(LifeComponent component)
        {
            this.animator.Play(stateName: "Death", layer: -1, normalizedTime: 0);
            this.animator.applyRootMotion = true;
            this.thirdPersonController.enabled = false;
            this.characterController.enabled = false;
        }
    }
}
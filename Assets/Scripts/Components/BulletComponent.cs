using System;
using System.Collections;
using Components;
using EnemySystem;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Sample
{
    [RequireComponent(typeof(TeamComponent))]
    [RequireComponent(typeof(Rigidbody))]
    public sealed class BulletComponent : MonoBehaviour
    {
        private TeamComponent teamComponent;

        [SerializeField]
        private int damage;
        
        [SerializeField]
        private float lifetime = 5.0f;
        
        [SerializeField]
        private float explosionRadius = 5f;
        
        [SerializeField]
        private float speed = 15.0f;
        
        [SerializeField]
        private float mineActivationTime = 3f;
        
        [SerializeField, Range(0, 1)]
        private float damageProbability = 0.8f;
        
        private bool isActive = false;

        public GameObject Source { get; set; }

        [SerializeField]
        private BulletTypes bulletType;
        
        private GameObjectPool pool;
        public void Construct(GameObjectPool bulletPool)
        {
            this.pool = bulletPool;
        }
        
        private void Awake()
        {
            this.teamComponent = this.GetComponent<TeamComponent>();
        }

        private void OnEnable()
        {
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        private void FixedUpdate()
        {
            if (bulletType is BulletTypes.Rocket)
            {
                this.GetComponent<Rigidbody>().AddForce(transform.forward * speed);
            }
            else if (bulletType is BulletTypes.Mine)
            {
                //this.GetComponent<Rigidbody>().AddForce(transform.up + (transform.forward / 3) * speed);
            }
            
            if (this.lifetime > 0)
            {
                this.lifetime -= Time.fixedDeltaTime;
            }
            else
            {
                Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
                foreach (Collider collider in colliders)
                {
                    // ����������, ����� ������� �� ����� �������� �� �����
                    if (collider.gameObject.layer == 6)
                    {
                        // ������� ����� �� �������, ������� ��� ����� � ������� ������
                        collider.GetComponent<LifeComponent>().TakeDamage(1000);
                    }
                    else
                    {
                        if (collider.gameObject.TryGetComponent(out Rigidbody rb))
                        {
                            rb.AddExplosionForce(5000, transform.forward, 300);
                        }
                    }
                }
                this.lifetime = 5;
                this.pool.Release(this.gameObject);
            }
        }

        private void Update()
        {
            if (bulletType is BulletTypes.Mine && !isActive)
            {
                StartCoroutine(ActivateMine());
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            var obj = collision.gameObject;

            if (!this.IsEnemy(obj))
            {
                if (collision.gameObject.TryGetComponent(out Rigidbody rb))
                {
                    rb.AddExplosionForce(5000, transform.forward, 300);
                }
                return;
            }
            
            float random = Random.Range(0.0f, 1.0f);
            if (random <= this.damageProbability)
            {
                DealDamage(obj);
            }

            this.pool.Release(this.gameObject);
        }

        private void DealDamage(GameObject obj)
        {
            if (!obj.TryGetComponent(out LifeComponent lifeComponent))
            {
                return;
            }
            
            lifeComponent.TakeDamage(this.damage);

            if (!lifeComponent.IsAlive && this.Source != null &&
                this.Source.TryGetComponent(out KillComponent killComponent))
            {
                killComponent.NotifyAboutKill(obj);
            }
        }

        private bool IsEnemy(GameObject other)
        {
            return other.TryGetComponent(out TeamComponent component) &&
                   component.Team != this.teamComponent.Team;
        }

        private IEnumerator ActivateMine()
        {
            yield return new WaitForSeconds(mineActivationTime);
            isActive = true;
        }
    }
}
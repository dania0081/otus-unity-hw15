using UnityEngine;

namespace Sample
{
    public enum TeamType
    {
        PLAYER = 0,
        ENEMY = 1
    }

    public sealed class TeamComponent : MonoBehaviour
    {
        [SerializeField]
        private TeamType team;

        public TeamType Team
        {
            get { return this.team; }
            set { this.team = value; }
        }
    }
}
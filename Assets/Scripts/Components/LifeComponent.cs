using System;
using UnityEngine;

namespace Sample
{
    public sealed class LifeComponent : MonoBehaviour
    {
        public event Action<LifeComponent> OnDeath;

        [SerializeField]
        private int maxHitPoints;

        [SerializeField]
        private int hitPoints;

        public int HitPoints
        {
            get { return this.hitPoints; }
        }

        public bool IsAlive
        {
            get { return this.hitPoints > 0; }
        }

        private void Awake()
        {
            this.SetFullHitPoints();
        }

        public void TakeDamage(int damage)
        {
            if (this.hitPoints == 0 || damage <= 0)
            {
                return;
            }

            this.hitPoints = Mathf.Max(this.hitPoints - damage, 0);

            if (this.hitPoints == 0)
            {
                this.OnDeath?.Invoke(this);
            }
        }
        
        public void SetFullHitPoints()
        {
            this.hitPoints = this.maxHitPoints;
        }
    }
}
using EnemySystem;
using UnityEngine;

namespace Sample
{
    [RequireComponent(typeof(TeamComponent))]
    public sealed class WeaponComponent : MonoBehaviour
    {
        [SerializeField]
        private float shootCountdown = 1.0f;

        private float shootTime;

        [SerializeField]
        private Transform firePoint;

        private bool fireRequired;

        [SerializeField, Range(0, 45.0f)]
        private float bulletSpreadAngle = 10.0f;

        private TeamComponent teamComponent;
        
        public BulletTypes bulletType { get; set; } = BulletTypes.Rocket;

        [SerializeField]
        private GameObjectPool rocketPool;
        [SerializeField]
        private GameObjectPool minePool;

        public void Construct(GameObjectPool bulletPool)
        {
            this.rocketPool = bulletPool;
        }

        public void Shoot()
        {
            this.fireRequired = true;
        }

        private void Awake()
        {
            this.teamComponent = this.GetComponent<TeamComponent>();
        }

        private void FixedUpdate()
        {
            if (!this.fireRequired)
            {
                return;
            }

            this.fireRequired = false;

            if (this.shootTime > 0)
            {
                this.shootTime -= Time.fixedDeltaTime;
                return;
            }

            this.SpawnBullet();
            this.shootTime = this.shootCountdown;
        }

        private void SpawnBullet()
        {
            //Vector3 firePosition = this.firePoint.position;
            //float randomSpread = Random.Range(-this.bulletSpreadAngle, this.bulletSpreadAngle);
            //Quaternion fireRotation = this.firePoint.rotation * Quaternion.AngleAxis(randomSpread, this.firePoint.forward);
            //Quaternion fireRotation = this.firePoint.localRotation;
            //Debug.Log(fireRotation.eulerAngles);
            GameObject bullet;
            if (bulletType is BulletTypes.Rocket)
            {
                bullet = this.rocketPool.Get(firePoint);
                bullet.GetComponent<BulletComponent>().Construct(this.rocketPool);
            }
            else
            {
                bullet = this.minePool.Get(firePoint);
                bullet.GetComponent<BulletComponent>().Construct(this.minePool);
            }
            //bullet.transform.rotation = fireRotation;

            bullet.GetComponent<TeamComponent>().Team = this.teamComponent.Team;
            bullet.GetComponent<BulletComponent>().Source = this.gameObject;
        }
    }
}
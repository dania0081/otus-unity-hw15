using System;
using System.Collections.Generic;
using UnityEngine;

namespace EnemySystem
{
    //Scene enemies
    public sealed class EnemyService : MonoBehaviour
    {
        public event Action<GameObject> OnEnemyAdded;
        public event Action<GameObject> OnEnemyRemoved;
        
        private readonly HashSet<GameObject> sceneEnemies = new();

        public int EnemyCount
        {
            get { return this.sceneEnemies.Count; }
        }

        private void Start()
        {
            var enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in enemies)
            {
                this.AddEnemy(enemy);
            }
        }

        public void AddEnemy(GameObject enemy)
        {
            if (this.sceneEnemies.Add(enemy))
            {
                this.OnEnemyAdded?.Invoke(enemy);
            }
        }

        public void RemoveEnemy(GameObject enemy)
        {
            if (this.sceneEnemies.Remove(enemy))
            {
                this.OnEnemyRemoved?.Invoke(enemy);
            }
        }
    }
}
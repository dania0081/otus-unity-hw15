using System;
using Sample;
using UnityEngine;
using Random = UnityEngine.Random;

namespace EnemySystem
{
    //TODO: Spawn enemies...
    public sealed class EnemySpawner : MonoBehaviour
    {
        [SerializeField]
        private EnemyService enemyService;
        
        [SerializeField]
        private GameObjectPool enemyPool;

        [SerializeField]
        private GameObjectPool bulletPool;
        
        [SerializeField, Space]
        private SpawnData[] spawnDataSet;

        [SerializeField]
        private float spawnPeriod = 1;

        [SerializeField]
        private float spawnCountdown;

        [SerializeField]
        private int enemyLimit = 3;


        private void Awake()
        {
            this.spawnCountdown = this.spawnPeriod;
        }

        private void FixedUpdate()
        {
            if (this.enemyService.EnemyCount >= this.enemyLimit)
            {
                return;
            }
            
            if (this.spawnCountdown > 0)
            {
                this.spawnCountdown -= Time.fixedDeltaTime;
                return;
            }
            
            this.SpawnEnemy();
            this.spawnCountdown = this.spawnPeriod;
        }

        private void SpawnEnemy()
        {
            int spawnIndex = Random.Range(0, this.spawnDataSet.Length);
            SpawnData spawnData = this.spawnDataSet[spawnIndex];

            Vector3 spawnPosition = spawnData.spawnPoint.position;
            Quaternion spawnRotation = spawnData.spawnPoint.rotation;

            var enemy = this.enemyPool.Get(spawnData.spawnPoint);
            
            enemy.GetComponentInChildren<NPCData>().patrolPoints = spawnData.patrolPoints;
            enemy.GetComponent<LifeComponent>().SetFullHitPoints();
            enemy.GetComponent<WeaponComponent>().Construct(this.bulletPool);
            this.enemyService.AddEnemy(enemy);
        }
        
        [Serializable]
        private struct SpawnData
        {
            [SerializeField]
            public Transform spawnPoint;

            [SerializeField]
            public Transform[] patrolPoints;
        }
    }
}
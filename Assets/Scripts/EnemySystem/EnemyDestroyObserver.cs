using Sample;
using UnityEngine;

namespace EnemySystem
{
    public sealed class EnemyDestroyObserver : MonoBehaviour
    {
        [SerializeField]
        private EnemyService enemyService;

        [SerializeField]
        private GameObjectPool enemyPool;

        private void OnEnable()
        {
            this.enemyService.OnEnemyAdded += this.OnEnemySpawn;
            this.enemyService.OnEnemyRemoved += this.OnEnemyUnspawn;
        }

        private void OnDisable()
        {
            this.enemyService.OnEnemyAdded -= this.OnEnemySpawn;
            this.enemyService.OnEnemyRemoved -= this.OnEnemyUnspawn;
        }

        private void OnEnemySpawn(GameObject enemy)
        {
            enemy.GetComponent<LifeComponent>().OnDeath += this.OnEnemyDeath;
        }

        private void OnEnemyUnspawn(GameObject enemy)
        {
            enemy.GetComponent<LifeComponent>().OnDeath -= this.OnEnemyDeath;
        }

        private void OnEnemyDeath(LifeComponent component)
        {
            var enemy = component.gameObject;
            this.enemyService.RemoveEnemy(enemy);
            this.enemyPool.Release(enemy);
        }
    }
}
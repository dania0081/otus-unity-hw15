using System.Collections.Generic;
using UnityEngine;

namespace EnemySystem
{
    public sealed class GameObjectPool : MonoBehaviour
    {
        [SerializeField]
        private int initialSize = 3;

        [SerializeField]
        private GameObject prefab;

        [SerializeField]
        private Transform container;

        [SerializeField]
        private Transform worldTransform;

        private readonly Queue<GameObject> queue = new();

        private void Awake()
        {
            for (int i = 0; i < this.initialSize; i++)
            {
                var enemy = Instantiate(this.prefab, this.container);
                this.queue.Enqueue(enemy);
            }
        }

        public GameObject Get(Transform spawnTransform)
        {
            if (this.queue.TryDequeue(out var spawnObject))
            {
                spawnObject.transform.SetParent(this.worldTransform);
                spawnObject.transform.position = spawnTransform.position;
                spawnObject.transform.rotation = spawnTransform.rotation;
                return spawnObject;
            }

            return Instantiate(this.prefab, spawnTransform.position, spawnTransform.rotation, this.worldTransform);
        }

        public void Release(GameObject enemy)
        {
            enemy.transform.SetParent(this.container);
            this.queue.Enqueue(enemy);
        }
    }
}